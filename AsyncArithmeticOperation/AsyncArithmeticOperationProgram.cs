﻿using System;
using System.Threading.Tasks;

namespace AsyncArithmeticOperation
{
    public class AsyncArithmeticOperationProgram
    {
        static void Main(string[] args)
        {
            Arithmetic();
            Console.WriteLine();
            Console.ReadLine();
        }
        /// <summary>
        /// Aysnchronous arithmetic operation.
        /// </summary>
        public static async void Arithmetic()
        {
            double firstNumber = 0, secondNumber = 0;

            try
            {
                Console.Write("Enter First Number: ");
                firstNumber = double.Parse(Console.ReadLine());

                Console.Write("Enter Second Number: ");
                secondNumber = double.Parse(Console.ReadLine());

            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(0);
            }
            finally
            {
                double add = await Add(firstNumber, secondNumber);
                Console.WriteLine("Add: " + add);

                double subtract = await Subtract(firstNumber, secondNumber);
                Console.WriteLine("Subtract: " + subtract);

                double multiply = await Multiply(firstNumber, secondNumber);
                Console.WriteLine("Multiply: " + multiply);

                double divide = await Divide(firstNumber, secondNumber);
                Console.WriteLine("Divide: " + divide);
            }
        }
        /// <summary>
        /// Aysnchronous addition method.
        /// </summary>
        /// <param name="num1">Double parameter</param>
        /// <param name="num2">Double parameter</param>
        /// <returns>Addition between two parameters.</returns>
        public static async Task<double> Add(double num1, double num2)
        {
            double add = 0;
            await Task.Run(() =>
            {
                add = num1 + num2;
            });
            return add;

        }
        /// <summary>
        /// Asynchronous subtration method
        /// </summary>
        /// <param name="num1">Double parameter</param>
        /// <param name="num2">Double parameter</param>
        /// <returns>Subtraction between two parameters.</returns>
        public static async Task<double> Subtract(double num1, double num2)
        {
            double subtract = 0;
            await Task.Run(() =>
            {
                subtract = num1 - num2;
            });

            return subtract;
        }
        /// <summary>
        /// Asynchronous multiplication method
        /// </summary>
        /// <param name="num1">Double parameter</param>
        /// <param name="num2">Double parameter</param>
        /// <returns>Multiplication between two parameters.</returns>
        public static async Task<double> Multiply(double num1, double num2)
        {
            double Multiply = 0;
            await Task.Run(() =>
            {
                Multiply = num1 * num2;
            });

            return Multiply;
        }
        /// <summary>
        /// Asynchronous division method
        /// </summary>
        /// <param name="num1">Double parameter</param>
        /// <param name="num2">Double parameter</param>
        /// <returns>Division between two parameters.</returns>
        public static async Task<double> Divide(double num1, double num2)
        {
            double Divide = 0;
            await Task.Run(() =>
            {
                Divide = Math.Round((num1) / (num2), 3);
            });

            return Divide;
        }
    }
}
