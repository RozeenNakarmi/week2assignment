﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdateCount
{
    /// <summary>
    /// Gets data from the csv files and store data into properties file.
    /// </summary>
    public class BirthDates
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public DateTime Dob { get; set; }
    }
}
