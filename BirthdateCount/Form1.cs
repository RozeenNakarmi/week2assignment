﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BirthdateCount
{
    public partial class Form1 : Form
    {
        public static List<BirthDates> lst = new List<BirthDates>();
        public Form1()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            LoadCsvFile();
            AssignLabel();
        }
        /// <summary>
        /// Loads csv file in DataGridView.
        /// </summary>
        public void LoadCsvFile()
        {
            try
            {
                string data = @".\iwbirthday.csv";
                if (File.Exists(data) && File.ReadLines(data).Count() > 0)
                {
                    StreamReader sr = new StreamReader(data);
                    string line;
                    string[] rows = new string[3];
                    while ((line = sr.ReadLine()) != null)
                    {
                        BirthDates birthdates = new BirthDates();
                        rows = line.Split(',');
                        birthdates.Name = rows[0].ToString();
                        birthdates.Date = rows[1].ToString();
                        birthdates.Dob = DateTime.Parse(rows[2].ToString());

                        lst.Add(birthdates);
                        Dataload();
                    }
                    sr.Close();
                }
                else
                {
                    MessageBox.Show("No Csv file.");
                }
    

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Binds data in datagridview.
        /// </summary>
        public void Dataload()
        {
            var binding = new BindingList<BirthDates>(lst);
            var source = new BindingSource(binding, null);
            dataGridView1.DataSource = source;
        }
        /// <summary>
        /// Calculates the number of people having birthdays in each month.
        /// </summary>
        public void AssignLabel()
        {

            label13.Text = "January";
            label14.Text = "February";
            label15.Text = "March";
            label16.Text = "April";
            label17.Text = "May";
            label18.Text = "June";
            label19.Text = "July";
            label20.Text = "August";
            label21.Text = "September";
            label22.Text = "October";
            label23.Text = "November";
            label24.Text = "December";
            TestClass tc = new TestClass();
            int flag = 0;
            foreach (var a in tc.CalculateDateQuery())
            {
                flag++;
                if (flag == 1)
                {
                    label1.Text = a.ToString();
                }
                else if (flag == 2)
                {
                    label2.Text = a.ToString();
                }
                else if (flag == 3)
                {
                    label3.Text = a.ToString();
                }
                else if (flag == 4)
                {
                    label4.Text = a.ToString();
                }else if (flag == 5)
                {
                    label5.Text = a.ToString();
                }
                else if (flag == 6)
                {
                    label6.Text = a.ToString();
                }
                else if (flag == 7)
                {
                    label7.Text = a.ToString();
                }
                else if (flag == 8)
                {
                    label8.Text = a.ToString();
                }
                else if (flag == 9)
                {
                    label9.Text = a.ToString();
                }
                else if (flag == 10)
                {
                    label10.Text = a.ToString();
                }
                else if (flag == 11)
                {
                    label11.Text = a.ToString();
                }
                else if (flag == 12)
                {
                    label12.Text = a.ToString();
                }
            }
        }
        /// <summary>
        /// Opens another windows form where same process is done in lamba expression.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            LambdaForm lambda = new LambdaForm();
            lambda.Show();
        }
        
    }
}
