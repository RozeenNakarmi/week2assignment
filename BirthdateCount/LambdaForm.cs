﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BirthdateCount
{
    public partial class LambdaForm : Form
    {
        public LambdaForm()
        {
            InitializeComponent();
            LoadGridview();
            AssignLabel();
        }
        /// <summary>
        /// Loads csv file in DataGridView.
        /// </summary>
        public void LoadGridview()
        {
            foreach (BirthDates bd in Form1.lst)
            {
                dataGridView1.Rows.Add(bd.Name, bd.Date, bd.Dob);
            }
        }
        /// <summary>
        /// Calculates the number of people having birthdays in each month.
        /// </summary>
        public void AssignLabel()
        {

            label13.Text = "January";
            label14.Text = "February";
            label15.Text = "March";
            label16.Text = "April";
            label17.Text = "May";
            label18.Text = "June";
            label19.Text = "July";
            label20.Text = "August";
            label21.Text = "September";
            label22.Text = "October";
            label23.Text = "November";
            label24.Text = "December";

            TestClass tc = new TestClass();
            int flag = 0;
            foreach (var a in tc.Data())
            {
                flag++;
                if (flag == 1)
                {
                    label1.Text = a.ToString();
                }
                else if (flag == 2)
                {
                    label2.Text = a.ToString();
                }
                else if (flag == 3)
                {
                    label3.Text = a.ToString();
                }
                else if (flag == 4)
                {
                    label4.Text = a.ToString();
                }
                else if (flag == 5)
                {
                    label5.Text = a.ToString();
                }
                else if (flag == 6)
                {
                    label6.Text = a.ToString();
                }
                else if (flag == 7)
                {
                    label7.Text = a.ToString();
                }
                else if (flag == 8)
                {
                    label8.Text = a.ToString();
                }
                else if (flag == 9)
                {
                    label9.Text = a.ToString();
                }
                else if (flag == 10)
                {
                    label10.Text = a.ToString();
                }
                else if (flag == 11)
                {
                    label11.Text = a.ToString();
                }
                else if (flag == 12)
                {
                    label12.Text = a.ToString();
                }
            }
        }
    }
}
