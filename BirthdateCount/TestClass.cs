﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BirthdateCount
{
    public class TestClass
    {
        private readonly List<int> contListquery = new List<int>();
        private readonly List<int> contListLambda = new List<int>();

        public List<int> CalculateDateQuery()
        {
            foreach (BirthDates bd in Form1.lst)
            {
                var jan = (from a in Form1.lst
                           where a.Dob.Month == 1
                           select a).Count();
                contListquery.Add(jan);

                var feb = (from a in Form1.lst
                           where a.Dob.Month == 2
                           select a).Count();
                contListquery.Add(feb);

                var march = (from a in Form1.lst
                             where a.Dob.Month == 3
                             select a).Count();
                contListquery.Add(march);

                var april = (from a in Form1.lst
                             where a.Dob.Month == 4
                             select a).Count();
                contListquery.Add(april);


                var may = (from a in Form1.lst
                           where a.Dob.Month == 5
                           select a).Count();
                contListquery.Add(may);

                var june = (from a in Form1.lst
                            where a.Dob.Month == 6
                            select a).Count();
                contListquery.Add(june);


                var july = (from a in Form1.lst
                            where a.Dob.Month == 7
                            select a).Count();
                contListquery.Add(july);


                var august = (from a in Form1.lst
                              where a.Dob.Month == 8
                              select a).Count();
                contListquery.Add(august);


                var september = (from a in Form1.lst
                                 where a.Dob.Month == 9
                                 select a).Count();
                contListquery.Add(september);


                var october = (from a in Form1.lst
                               where a.Dob.Month == 10
                               select a).Count();
                contListquery.Add(october);


                var november = (from a in Form1.lst
                                where a.Dob.Month == 11
                                select a).Count();
                contListquery.Add(november);


                var december = (from a in Form1.lst
                                where a.Dob.Month == 12
                                select a).Count();
                contListquery.Add(december);
            }
            return contListquery;
        }
        public void CalculateDateLambda()
        {
            foreach (BirthDates bd in Form1.lst)
            {

                int january = Form1.lst.Count(x => x.Dob.Month == 1);
                contListLambda.Add(january);

                int february = Form1.lst.Count(x => x.Dob.Month == 2);
                contListLambda.Add(february);


                int march = Form1.lst.Count(x => x.Dob.Month == 3);
                contListLambda.Add(march);


                int april = Form1.lst.Count(x => x.Dob.Month == 4);
                contListLambda.Add(april);


                int may = Form1.lst.Count(x => x.Dob.Month == 5);
                contListLambda.Add(may);


                int june = Form1.lst.Count(x => x.Dob.Month == 6);
                contListLambda.Add(june);


                int july = Form1.lst.Count(x => x.Dob.Month == 7);
                contListLambda.Add(july);


                int august = Form1.lst.Count(x => x.Dob.Month == 8);
                contListLambda.Add(august);


                int september = Form1.lst.Count(x => x.Dob.Month == 9);
                contListLambda.Add(september);


                int october = Form1.lst.Count(x => x.Dob.Month == 10);
                contListLambda.Add(october);

                int november = Form1.lst.Count(x => x.Dob.Month == 11);
                contListLambda.Add(november);


                int dec = Form1.lst.Count(x => x.Dob.Month == 12);
                contListLambda.Add(dec);
            }
        }
        public List<int> Data()
        {
            return contListLambda;

        }

    }
}
