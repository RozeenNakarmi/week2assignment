﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BirthdayCountConsole
{
    public class BirthdayCountConsoleProgram
    {
        public static List<BirthDates> lst = new List<BirthDates>();
        static void Main(string[] args)
        {
            /*Load CSV File and it in list*/
            string data = @".\iwbirthday.csv";
            if (File.Exists(data) && File.ReadLines(data).Count() > 0)
            {
                StreamReader sr = new StreamReader(data);
                string line;
                string[] rows = new string[3];
                while ((line = sr.ReadLine()) != null)
                {
                    BirthDates birthdates = new BirthDates();
                    rows = line.Split(',');
                    birthdates.Name = rows[0].ToString();
                    birthdates.Date = rows[1].ToString();
                    birthdates.Dob = DateTime.Parse(rows[2].ToString());
                    lst.Add(birthdates);
                }
                sr.Close();
            }
            else
            {
                Console.WriteLine("No CSV File");
            }
            BirthdayCountConsoleProgram program = new BirthdayCountConsoleProgram();
            program.LambdaExpression();
            program.QueryExpression();
            
        }
        /// <summary>
        /// Query Expression to calculate number of birthdate in each month
        /// </summary>
        /// <returns>Month and total number of people having birthdates in each month.</returns>
        public Dictionary<string,int> QueryExpression()
        {
            Console.WriteLine();
            Console.WriteLine("Query Expression");
            var queryExpressionBirthdayCount = from x in lst
                                               group x.Dob.ToString("MMMM") by x.Dob.ToString("MMMM") into g
                                               select new { chars = g.Key, count = g.Count() };
            foreach (var g in queryExpressionBirthdayCount)
            {
                Console.WriteLine("{0} => {1}", g.chars, g.count);
            }
            return queryExpressionBirthdayCount.ToDictionary(x => x.chars, y=> y.count);
        }
        /// <summary>
        /// Lambda Expression to calculate number of birthdate in each month
        /// </summary>
        /// <returns>Month and total number of people having birthdates in each month.</returns>
        public Dictionary<string,int> LambdaExpression()
        {
            var lambdaExpressionBirthdateCount = lst.Select(x => x.Dob.ToString("MMMM"))
                            .GroupBy(x => x)
                            .ToDictionary(grp => grp.Key, grp => grp.Count());
 

            Console.WriteLine("Lambda Expression");
            foreach (var birthdayData in lambdaExpressionBirthdateCount)
            {
                Console.WriteLine("{0} => {1}", birthdayData.Key, birthdayData.Value);

            }
            return lambdaExpressionBirthdateCount;
        }
    }
}
