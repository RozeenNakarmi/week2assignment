﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EachLetterCalculation
{
    public class EachLetterCalculationProgram
    {
        /// <summary>
        /// Calculate the number of each letter in a list
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public SortedDictionary<TItem, int> GetFrequencies<TItem>(IEnumerable<TItem> items)
        {
            var dictionary = new SortedDictionary<TItem, int>();
            foreach (var item in items)
            {
                if (dictionary.ContainsKey(item))
                {
                    dictionary[item]++;
                }
                else
                {
                    dictionary[item] = 1;
                }
            }
            foreach (var entry in dictionary)
            {
                Console.WriteLine("{0} => {1}", entry.Key, entry.Value);

            }
            return dictionary;
        }
        public static void Main(string[] args)
        {
            var m = new EachLetterCalculationProgram();
            //Console.Write("Enter any word: ");
            //string str = Console.ReadLine();
            Console.WriteLine("----------------------- \nQuery Expression");
            m.QueryExpression("Ram");

            Console.WriteLine("----------------------- \nLambda Expression");
            m.LambdaExpression("Ram");

            Console.WriteLine("----------------------- \nDictionary");
            m.GetFrequencies("Ram");

        }
        public Dictionary<char, int> LambdaExpression(string word)
        {
            Dictionary<char, int> counts = word.Where(char.IsLetter)
                .GroupBy(c => c)
                .OrderBy(c => c.Key)
                .ToDictionary(grp => grp.Key, grp => grp.Count());

            foreach (var b in counts)
            {
                Console.WriteLine("{0} => {1}", b.Key, b.Value);

            }
            return counts;
        }
        public Dictionary<char, int> QueryExpression(string word)
        {
            var freq = from c in word
                       where char.IsLetter(c)
                       orderby c
                       group c by c into g
                       select new { charac = g.Key, count = g.Count() };
            foreach (var g in freq)
            {
                Console.WriteLine("{0} => {1}", g.charac, g.count);

            }
            return freq.ToDictionary(x=> x.charac,y=> y.count );
        }
    }
}
