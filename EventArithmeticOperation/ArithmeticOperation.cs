﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventArithmeticOperation
{
    /// <summary>
    /// Sender class of synchronous arthmetic operation
    /// </summary>
    public class ArithmeticOperation
    {
        public delegate double ArthmeticOperationDelegate(object sender, Numbers n);

        public event ArthmeticOperationDelegate OperationEvent;

        /// <summary>
        /// Invokes two double parameters from OperationEvent event handler.
        /// </summary>
        /// <param name="number1">Returns double</param>
        /// <param name="number2">Returns double</param>
        public void OnReach(double number1, double number2)
        {
            OperationEvent?.Invoke(this, new Numbers() { Num1 = number1, Num2 = number2 });
        }
    }
}
