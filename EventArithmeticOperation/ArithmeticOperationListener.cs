﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventArithmeticOperation
{
    /// <summary>
    /// Listener class of synchronous arthmetic operation
    /// </summary>
    public class ArithmeticOperationListener
    {
        /// <summary>
        /// Arithmetic operation like addition, subtraction, multiplication and division is operated.
        /// </summary>
        /// <param name="ao">It is an object of ArithmeticOperation class.</param>
        public void ListenArthmetic(ArithmeticOperation ao)
        {
            //ao.OperationEvent += (delegate (object sender, Numbers n)
            //{
            //    //addition
            //    double addition = n.Num1 + n.Num2;
            //    Console.WriteLine("Addition = " + addition);

            //    //division
            //    double division = n.Num1 / n.Num2;
            //    Console.WriteLine("Division = " + division);

            //    //multiplication
            //    double multiplication = n.Num1 * n.Num2;
            //    Console.WriteLine("Multiplication = " + multiplication);

            //    //subtraction
            //    double subtraction = n.Num1 - n.Num2;
            //    Console.WriteLine("Subtraction = " + subtraction);
            //});

            ao.OperationEvent += Addition;
            ao.OperationEvent += Subtraction;
            ao.OperationEvent += Multiplication;
            ao.OperationEvent += Division;
        }

        public double Division(object sender, Numbers n)
        {
            double division = n.Num1 / n.Num2;
            Console.WriteLine("Division = " + division);
            return division; 

        }
       
        public double Multiplication(object sender, Numbers n)
        {
            double multiplication = n.Num1 * n.Num2;
            Console.WriteLine("Multiplication = " + multiplication);
            return multiplication;
        }

        public double Subtraction(object sender, Numbers n)
        {
            double subtraction = n.Num1 - n.Num2;
            Console.WriteLine("Subtraction = " + subtraction);
            return subtraction;
        }

        public double Addition(object sender, Numbers n)
        {
            double addition= n.Num1 + n.Num2;
            Console.WriteLine("Addition = " + addition);
            return addition;
        }
    }
}

