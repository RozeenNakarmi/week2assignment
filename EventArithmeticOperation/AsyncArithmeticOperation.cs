﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EventArithmeticOperation
{
    /// <summary>
    /// Sender class of asynchronous arthmetic operation
    /// </summary>
    public class AsyncArithmeticOperation
    {
        public delegate Task AsyncArthmeticOpetion(Object sender, Numbers n);

        public event AsyncArthmeticOpetion AysncOperationEvent;
        /// <summary>
        /// Invokes two double parameters from AysncOperationEvent event handler. 
        /// </summary>
        /// <param name="number1"></param>
        /// <param name="number2"></param>
        /// <returns></returns>
        public async Task AsyncOnReach(double number1, double number2)
        {
            await Task.Run(() =>
            {
                AysncOperationEvent?.Invoke(this, new Numbers() { Num1 = number1, Num2 = number2 });
            });

        }
    }
}
