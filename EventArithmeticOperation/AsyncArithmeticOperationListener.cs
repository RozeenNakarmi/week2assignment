﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EventArithmeticOperation
{
    /// <summary>
    /// Listener class of asynchronous arthmetic operation
    /// </summary>
    public class AsyncArithmeticOperationListener
    {
        //List<double> li = new List<double>();
        /// <summary>
        /// Asyncronous arithmetic operation like addition, subtraction, multiplication and division is operated.
        /// </summary>
        /// <param name="ao">It is an object of AsyncArithmeticOperation class.</param>
        public void AsyncListenArthmetic(AsyncArithmeticOperation ao)
        {

            //ao.AysncOperationEvent += async (object sender, Numbers n) => await Task.Run(() => {
            //    //Addition
            //    double addition = n.Num1 + n.Num2;
            //    li.Add(addition);
            //    Console.WriteLine("Addition = " + addition);

            //    //Subtraction
            //    double subtraction = n.Num1 - n.Num2;
            //    li.Add(subtraction);
            //    Console.WriteLine("Subtraction = " + subtraction);

            //    //Multiplication
            //    double multiplication = n.Num1 * n.Num2;
            //    li.Add(multiplication);
            //    Console.WriteLine("Multiplication = " + multiplication);

            //    //Division
            //    double division = n.Num1 / n.Num2;
            //    li.Add(division);
            //    Console.WriteLine("Division = " + division);
            //});

            ao.AysncOperationEvent += Addition;
            ao.AysncOperationEvent += Subtraction;
            ao.AysncOperationEvent += Multiplication;
            ao.AysncOperationEvent += Division;


        }

        public async Task<double> Division(object sender, Numbers n)
        {
            double division = 0;
            await Task.Run(() =>
            {
                division = n.Num1 / n.Num2;
                Console.WriteLine("Division = " + division);

            });
            return division;
        }

        public async Task<double> Multiplication(object sender, Numbers n)
        {
            double multiplication = 0;
            await Task.Run(()=> {
                multiplication = n.Num1 * n.Num2;
                Console.WriteLine("Multiplication = " + multiplication);
            });
            return multiplication;
        }

        public async Task<double> Subtraction(object sender, Numbers n)
        {
            double subtraction = 0;
            await Task.Run(() =>
            {
                subtraction = n.Num1 - n.Num2;
                Console.WriteLine("Subtraction = " + subtraction);
            });
            return subtraction;
        }

        public async Task<Double> Addition(object sender, Numbers n)
        {
            double addition = 0;
            await Task.Run(() =>
            {
                addition = n.Num1 + n.Num2;
                Console.WriteLine("Addition = " + addition);
            });
            return addition;
        }
    }
}
