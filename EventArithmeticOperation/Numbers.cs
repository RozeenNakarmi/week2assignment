﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventArithmeticOperation
{
    /// <summary>
    /// Property class for two double number.
    /// </summary>
   public class Numbers
   {
        public double Num1 { get; set; }
        public double Num2 { get; set; }
   }
}
