﻿using System;

namespace EventArithmeticOperation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            double a = 0;
            double b = 0;
            try
            {
                Console.Write("Enter your first number: ");
                a = double.Parse(Console.ReadLine());
                Console.Write("Enter your Second number: ");
                b = double.Parse(Console.ReadLine());
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(0);

            }
            finally
            {
                Console.WriteLine("\nSync Method");
                var watch = System.Diagnostics.Stopwatch.StartNew();

                ArithmeticOperation ao = new ArithmeticOperation();
                ArithmeticOperationListener al = new ArithmeticOperationListener();
                al.ListenArthmetic(ao);

                ao.OnReach(a, b);
                watch.Stop();
                Console.WriteLine(watch.Elapsed);
                Console.WriteLine("-----------------\nAsync Method");
                var watch2 = System.Diagnostics.Stopwatch.StartNew();

                AsyncArithmeticOperation aao = new AsyncArithmeticOperation();
                AsyncArithmeticOperationListener aal = new AsyncArithmeticOperationListener();
                aal.AsyncListenArthmetic(aao);

                aao.AsyncOnReach(a, b).Wait();
                watch2.Stop();
                Console.WriteLine(watch2.Elapsed);
            }
            Console.ReadKey();


        }
    }
}
