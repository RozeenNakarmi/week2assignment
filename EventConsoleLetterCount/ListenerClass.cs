﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventConsoleLetterCount
{
    public class ListenerClass
    {
        int count = 0;

        /// <summary>
        /// Counts character when ever WriteEvent is invoked.
        /// Delete character from the list and count is decreased when ever BackSpaceEvent is invoked
        /// </summary>
        /// <param name="senderClass">Object of SenderClass</param>
        public void Listen(SenderClass senderClass)
        {
            //senderClass.WriteEvent += delegate (object sender, PropertyClass propertyClass)
            //{

            //    Console.SetCursorPosition(112, 0);

            //    count++;
            //    Console.WriteLine(count);

            //};
            senderClass.WriteEvent += LetterCount;
            senderClass.BackSpaceEvent += BackSpace;
            
            //senderClass.BackSpaceEvent += delegate (object sender, PropertyClass property)
            //{
            //    int x = Console.CursorLeft;
            //    int y = Console.CursorTop;
            //    List<ConsoleKey> keys = property.Lst;
            //    keys.Reverse();
            //    foreach (ConsoleKey ck in keys)
            //    {
            //        if (!ck.Equals(ConsoleKey.Spacebar) && !ck.Equals(ConsoleKey.Enter) && !ck.Equals(ConsoleKey.Tab))
            //        {
            //            count--;
            //            keys.Reverse();
            //            break;
            //        }
            //    }
            //    if (x == 0 && y > 0)
            //    {
            //        Console.SetCursorPosition(x, y);
            //        Console.Write("\b \b");
            //        Console.SetCursorPosition(110, y - 1);
            //        Console.Write("\b \b");
            //    }
            //    else
            //    {
            //        Console.SetCursorPosition(x + 1, y);
            //        Console.Write("\b \b");
            //    }
            //    Console.SetCursorPosition(113, 0);
            //    Console.Write("\b \b");
            //    Console.SetCursorPosition(114, 0);
            //    Console.Write("\b \b");
            //    Console.SetCursorPosition(115, 0);
            //    Console.Write("\b \b");
            //    Console.SetCursorPosition(116, 0);
            //    Console.Write("\b \b");
            //    Console.SetCursorPosition(112, 0);
            //    Console.Write(count);
            //};
        }

        private void BackSpace(object sender, PropertyClass a)
        {
            int x = Console.CursorLeft;
            int y = Console.CursorTop;
            List<ConsoleKey> keys = a.Lst;
            keys.Reverse();
            foreach (ConsoleKey ck in keys)
            {
                if (!ck.Equals(ConsoleKey.Spacebar) && !ck.Equals(ConsoleKey.Enter) && !ck.Equals(ConsoleKey.Tab))
                {
                    count--;
                    keys.Reverse();
                    break;
                }
            }
            if (x == 0 && y > 0)
            {
                Console.SetCursorPosition(x, y);
                Console.Write("\b \b");
                Console.SetCursorPosition(110, y - 1);
                Console.Write("\b \b");
            }
            else
            {
                Console.SetCursorPosition(x + 1, y);
                Console.Write("\b \b");
            }
            Console.SetCursorPosition(113, 0);
            Console.Write("\b \b");
            Console.SetCursorPosition(114, 0);
            Console.Write("\b \b");
            Console.SetCursorPosition(115, 0);
            Console.Write("\b \b");
            Console.SetCursorPosition(116, 0);
            Console.Write("\b \b");
            Console.SetCursorPosition(112, 0);
            Console.Write(count);
        }

        private void LetterCount(object sender, PropertyClass a)
        {
            Console.SetCursorPosition(112, 0);

            count++;
            Console.WriteLine(count);
        }
    };
}
