﻿using System;
using System.Collections.Generic;

namespace EventConsoleLetterCount
{
    public class Program
    {
        static void Main(string[] args)
        {
            SenderClass send = new SenderClass();
            ListenerClass listen = new ListenerClass();
            listen.Listen(send);

            int x = 0;
            int y = 0;
            List<ConsoleKey> lst = new List<ConsoleKey>();
            
            while (true)
            {
                Console.SetCursorPosition(x, y);
                var letters = Console.ReadKey().Key;
                if (letters != ConsoleKey.Backspace)
                {
                    lst.Add(letters);
                }
                if (letters == ConsoleKey.Backspace)
                {
                    if (lst.Count == 0)
                    {
                        continue;
                    }

                    if (x == 0)
                    {
                        x = 110;
                        y--;
                    }
                    x--;
                    send.CallBackSpace(letters, lst);
                    lst.RemoveAt(lst.Count - 1);
                }
                else if (letters == ConsoleKey.Enter || letters == ConsoleKey.Tab || letters == ConsoleKey.Escape || letters == ConsoleKey.Spacebar)
                {
                    lst.RemoveAt(lst.Count - 1);
                    continue;
                }
                else 
                {
                    send.CallWrite(letters);
                    x++;
                }
                if (x % 110 == 0 && x != 0)
                {
                    y++;
                    x = 0;
                }

            }
        }
    }
}
