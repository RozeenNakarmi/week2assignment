﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventConsoleLetterCount
{
    /// <summary>
    /// Gets data from the event handler and store data into properties file.
    /// </summary>
    public class PropertyClass : EventArgs
    {
        public ConsoleKey Item { get; set; }

        public List<ConsoleKey> Lst { get; set; }
    }
}
