﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventConsoleLetterCount
{
    /// <summary>
    /// Sender class.
    /// </summary>
    public class SenderClass
    {
        public delegate void ConsoleLetterCount(Object sender, PropertyClass a);

        public event ConsoleLetterCount WriteEvent;
        public event ConsoleLetterCount BackSpaceEvent;

        /// <summary>
        /// Invokes consolekey from WriteEvent event handler.
        /// </summary>
        /// <param name="consolekey"></param>
        public void CallWrite(ConsoleKey consolekey)
        {
            WriteEvent?.Invoke(this, new PropertyClass() { Item = consolekey });
        }
        /// <summary>
        /// Invokes consolekey and list from BackSpaceEvent event handler.
        /// </summary>
        /// <param name="consoleKey"></param>
        /// <param name="listConsole"></param>
        public void CallBackSpace(ConsoleKey consoleKey, List<ConsoleKey> listConsole)
        {
            BackSpaceEvent?.Invoke(this, new PropertyClass() { Item = consoleKey, Lst = listConsole });
        }
    }
}
