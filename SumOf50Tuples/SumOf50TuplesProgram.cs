﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SumOf50Tuples
{
    public class SumOf50TuplesProgram
    {
        static async Task Main(string[] args)
        {
            List<Tuple<int, int>> lst = new List<Tuple<int, int>>();
            for (int i = 0; i < 50; i++)
            {
                lst.Add(Tuple.Create(i, i + 30));
            }
            /** Synchronous method.**/
            var watch = Stopwatch.StartNew();
            Console.WriteLine("Synchronous method.");
            Tuple<int, int> sum = AddTuples(lst);
            watch.Stop();
            Console.WriteLine("The sum of first component is " + sum.Item1);
            Console.WriteLine("The sum of second component is " + sum.Item2);
            Console.WriteLine("Time taken for synchronos method: "+ watch.Elapsed + "\n");
            
            /** Asynchronous method.**/
            var watch2 = Stopwatch.StartNew();
            Console.WriteLine("Asynchronous method.");
            Tuple<int, int> sum2 = await AddTuplesAsync(lst);
            watch.Stop();
            Console.WriteLine("The sum of first component is " + sum2.Item1);
            Console.WriteLine("The sum of second component is " + sum2.Item2);
            Console.WriteLine("Time taken for synchronos method: " + watch2.Elapsed + "\n");
            Console.ReadLine();
        }
        /// <summary>
        /// Synchronous sum of two integers tuples.
        /// </summary>
        /// <param name="lst">Object of list of two integers tuples.</param>
        /// <returns>The sum of two tuples.</returns>
        public static Tuple<int, int> AddTuples(List<Tuple<int, int>> lst)
        {
            int sum1 = 0;
            int sum2 = 0;
            foreach (Tuple<int, int> t in lst)
            {
                sum1 += t.Item1;
                sum2 += t.Item2;
            }
            Tuple<int, int> sum = Tuple.Create(sum1, sum2);
            return sum;
        }

        /// <summary>
        /// Asynchronous sum of two integers tuples.
        /// </summary>
        /// <param name="lst">Object of list of two integers tuples.</param>
        /// <returns>The sum of two tuples.</returns>
        public static async Task<Tuple<int, int>> AddTuplesAsync(List<Tuple<int, int>> lst)
        {
            int sum1 = 0;
            int sum2 = 0;
            await Task.Run(() =>
            {
                foreach (Tuple<int, int> t in lst)
                {
                    sum1 += t.Item1;
                    sum2 += t.Item2;
                }
            });
            Tuple<int, int> sum = Tuple.Create(sum1, sum2);
            return sum;
        }
       

    }
}
