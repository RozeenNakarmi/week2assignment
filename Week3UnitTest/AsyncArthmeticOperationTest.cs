using AsyncArithmeticOperation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Week3UnitTest
{
    [TestClass]
    public class AsyncArthmeticOperationTest
    {
        [TestMethod]
        public async Task AddTest_Scenario_MustAddTwoNumbers()
        {
            var additionResult = await AsyncArithmeticOperationProgram.Add(3, 4);
            Assert.AreEqual(7, additionResult);
            
        }
        [TestMethod]
        public async Task SubtractTest_Scenario_MustSubtractTwoNumbers()
        {
            var subtractResult = await AsyncArithmeticOperationProgram.Subtract(4, 3);
            Assert.AreEqual(1, subtractResult);
        }
        [TestMethod]
        public async Task MultiplyTest_Scenario_MustMultiplyTwoNumbers()
        {
            var multiplyResult = await AsyncArithmeticOperationProgram.Multiply(4, 2);
            Assert.AreEqual(8, multiplyResult);
        }
        [TestMethod]
        public async Task DivideTest_Scenario_MustDivideTwoNumbers()
        {
            var divideResult = await AsyncArithmeticOperationProgram.Divide(4, 2);
            Assert.AreEqual(2, divideResult);
        }

    }
}
