﻿using BirthdateCount;
using BirthdayCountConsole;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Week3UnitTest
{
    [TestClass]
    public class BirthdayCountTest
    {
        [TestMethod]
        public void QueryExpressionCountTest()
        {
            var queryTest = new BirthdayCountConsoleProgram();
            Dictionary<string, int> dict = new Dictionary<string, int>
            {
                {
                    "January",6
                }
            };
            Dictionary<string, int> result = queryTest.QueryExpression();
            for (int count = 0; count == 1; count++)
            {
                Assert.IsTrue(dict.ElementAt(count).Key.Equals(result.ElementAt(count).Key));
                Assert.IsTrue(dict.ElementAt(count).Value.Equals(result.ElementAt(count).Value));

            }

        }
        [TestMethod]
        public void LambdaExpressionTest()
        {
            var lambdaTest = new BirthdayCountConsoleProgram();
            Dictionary<string, int> dict = new Dictionary<string, int>
            {
                {
                    "December",9
                }
            };
            Dictionary<string, int> result = lambdaTest.LambdaExpression();
            for (int count = 0; count == 12; count++)
            {
                Assert.IsTrue(dict.ElementAt(count).Key.Equals(result.ElementAt(count).Key));
                Assert.IsTrue(dict.ElementAt(count).Value.Equals(result.ElementAt(count).Value));

            }
        }
    }
}
