﻿using EachLetterCalculation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Week3UnitTest
{
    [TestClass]
    public class EachLetterCalculationTest
    {
        [TestMethod]
        public void QueryExpressionTest()
        {
            Dictionary<char, int> dict = new Dictionary<char, int>
            {
                {
                    'R',1
                },
                {
                    'a',1
                },
                {
                    'm',1
                }
            };
          

            var m = new EachLetterCalculationProgram();
            Dictionary<char,int> result = m.QueryExpression("Ram");
            for (int count = 0; count <3; count ++)
            {
                Assert.IsTrue(dict.ElementAt(count).Key.Equals(result.ElementAt(count).Key));
                Assert.IsTrue(dict.ElementAt(count).Value.Equals(result.ElementAt(count).Value));

            }

        }
        [TestMethod]
        public void LamdaExpressionTest()
        {
            Dictionary<char, int> dict = new Dictionary<char, int>
            {
                {
                    'R',1
                },
                {
                    'a',1
                },
                {
                    'm',1
                }
            };

            var m = new EachLetterCalculationProgram();
            Dictionary<char, int> result = m.LambdaExpression("Ram");
            for (int count = 0; count < 3; count++)
            {
                Assert.IsTrue(dict.ElementAt(count).Key.Equals(result.ElementAt(count).Key));
                Assert.IsTrue(dict.ElementAt(count).Value.Equals(result.ElementAt(count).Value));

            }
        }
        [TestMethod]
        public void DictionaryTest()
        {
            Dictionary<char, int> dict = new Dictionary<char, int>
            {
                {
                    'R',1
                },
                {
                    'a',1
                },
                {
                    'm',1
                }
            };

            var m = new EachLetterCalculationProgram();
            SortedDictionary<char, int> result = m.GetFrequencies("Ram");
            for (int count = 0; count < 3; count++)
            {
                Assert.IsTrue(dict.ElementAt(count).Key.Equals(result.ElementAt(count).Key));
                Assert.IsTrue(dict.ElementAt(count).Value.Equals(result.ElementAt(count).Value));

            }
        }
    }
}
