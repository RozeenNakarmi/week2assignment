﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using EventArithmeticOperation;
using System.Threading.Tasks;

namespace Week3UnitTest
{
    [TestClass]
   public class EventArithmeticOperationTest
   {
        [TestMethod]
        public void EventSyncAddition_Scenario_Add2Numbers()
        {
            ArithmeticOperationListener ao = new ArithmeticOperationListener();
            var result = ao.Addition(null,new Numbers() { Num1 = 2, Num2 =3 }) ;

            Assert.AreEqual(5,result);
        }
        [TestMethod]
        public void EventSyncSubtraction_Scenario_Subtract2Numbers()
        {
            ArithmeticOperationListener ao = new ArithmeticOperationListener();
            var result = ao.Subtraction(null, new Numbers() { Num1 = 3, Num2 = 2 });

            Assert.AreEqual(1, result);
        }
        [TestMethod]
        public void EventSyncMultiplication_Scenario_Multiply2Numbers()
        {
            ArithmeticOperationListener ao = new ArithmeticOperationListener();
            var result = ao.Multiplication(null, new Numbers() { Num1 = 3, Num2 = 2 });

            Assert.AreEqual(6, result);
        }
        [TestMethod]
        public void EventSyncDivision_Scenario_Divide2Numbers()
        {
            ArithmeticOperationListener ao = new ArithmeticOperationListener();
            var result = ao.Division(null, new Numbers() { Num1 = 6, Num2 = 2 });

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public async Task EventAsyncAddition_Scenario_Add2Numbers()
        {
            AsyncArithmeticOperationListener ao = new AsyncArithmeticOperationListener();
            var result = await ao.Addition(null, new Numbers() { Num1 = 2, Num2 = 3 });
            Assert.AreEqual(5, result);

        }
        [TestMethod]
        public async Task EventAsyncSubtration_Scenario_Subtract2Numbers()
        {
            AsyncArithmeticOperationListener ao = new AsyncArithmeticOperationListener();
            var result = await ao.Subtraction(null, new Numbers() { Num1 = 3, Num2 = 2 });
            Assert.AreEqual(1, result);
        }
        [TestMethod]
        public async Task EventAsyncMultiplication_Scenario_Multiply2Numbers()
        {
            AsyncArithmeticOperationListener ao = new AsyncArithmeticOperationListener();
            var result = await ao.Multiplication(null, new Numbers() { Num1 = 3, Num2 = 2 });
            Assert.AreEqual(6, result);
        }
        [TestMethod]
        public async Task EventAsyncDivision_Scenario_Divide2Numbers()
        {
            AsyncArithmeticOperationListener ao = new AsyncArithmeticOperationListener();
            var result = await ao.Division(null, new Numbers() { Num1 = 2, Num2 = 2 });
            Assert.AreEqual(1, result);

        }

    }
}
