﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumOf50Tuples;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Week3UnitTest
{
    [TestClass]
    public class SumOf50TuplesTest
    {
        [TestMethod]
        public void Sync_SumOf50Tuples()
        {
            List<Tuple<int, int>> lst = new List<Tuple<int, int>>();
            for (int i = 0; i < 50; i++)
            {
                lst.Add(Tuple.Create(1, 2));
            }
            Tuple<int, int> sum = SumOf50TuplesProgram.AddTuples(lst);
            Assert.AreEqual(sum,Tuple.Create(50,100));

        }

        [TestMethod]
        public async Task Async_SumOf50Tuples()
        {
            List<Tuple<int, int>> lst = new List<Tuple<int, int>>();
            for (int i = 0; i < 50; i++)
            {
                lst.Add(Tuple.Create(1, 2));
            }
            Tuple<int, int> sum2 = await SumOf50TuplesProgram.AddTuplesAsync(lst);

            Assert.AreEqual(sum2, Tuple.Create(50, 100));
        }

    }
}
